﻿using Newtonsoft.Json.Linq;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.MySql;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Obfuscator
{



    class Program
    {

        private const string USER_ACCOUNT_OBF_TABLE = @"    DROP TABLE IF EXISTS  production.UserAccount_Obf;
                                                            CREATE TABLE production.UserAccount_Obf
                                                            SELECT *
                                                            FROM
                                                            production.UserAccount
                                                            WHERE user_email NOT LIKE '%@scileads.com';";

        private const string USER_TEMP_TABLE = @"   DROP TABLE IF EXISTS  `production`.`user_temp`;
                                                    CREATE TABLE `production`.`user_temp` (
                                                        `user_id` int (11) NOT NULL AUTO_INCREMENT,
                                                        `user_company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                                        `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NULL,
                                                        `new_user_company_name` varchar(255) COLLATE utf8mb4_unicode_ci NULL,
                                                        `new_user_email` varchar(255) COLLATE utf8mb4_unicode_ci NULL,
                                                        PRIMARY KEY(`user_id`)
                                                     ) ENGINE=InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COLLATE=utf8mb4_unicode_ci;

                                                    INSERT INTO production.user_temp (user_company_name, user_email, new_user_company_name, new_user_email)
                                                    SELECT user_company_name, user_email, '', RIGHT(user_email,  ( LENGTH(user_email) - POSITION('@' IN user_email) ) )
                                                    FROM production.UserAccount_Obf 
                                                    ";

        private const string COMPANY_TEMP_TABLE = @"DROP TABLE IF EXISTS  `production`.`company_temp`;
                                                    CREATE TABLE `production`.`company_temp` (
                                                      `company_id` int(11) NOT NULL AUTO_INCREMENT,
                                                      `user_company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                                      `new_user_company_name` varchar(255) COLLATE utf8mb4_unicode_ci NULL,
                                                      PRIMARY KEY (`company_id`)
                                                    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
                                                    
                                                    INSERT INTO production.company_temp (user_company_name, new_user_company_name)
                                                    select DISTINCT user_company_name, '' from production.user_temp;";

        private const string GENERATE_NEW_COMPANY_NAMES = @"UPDATE production.company_temp 
                                                            SET new_user_company_name = CONCAT('Company_', company_id);

                                                            UPDATE production.user_temp u , production.company_temp c
                                                            SET u.new_user_company_name = c.new_user_company_name
                                                            WHERE u.user_company_name = c.user_company_name";

        private const string GENERATE_NEW_COMPANY_EMAIL = @"UPDATE production.user_temp
                                                            SET new_user_email = CONCAT('User_' , user_id, '@', new_user_company_name, '.com')";


        private const string UPDATE_OBF_TABLE = @"  UPDATE production.UserAccount_Obf o, production.user_temp u
                                                    set o.user_company_name = u.new_user_company_name
                                                    where o.user_company_name = u.user_company_name;

                                                    update production.UserAccount_Obf o, production.user_temp u
                                                    set o.user_email = u.new_user_email
                                                    where o.user_email = u.user_email;

                                                    UPDATE production.UserAccount_Obf
                                                    SET user_password = 'NOT_USED';

                                                    UPDATE production.UserAccount_Obf
                                                    SET user_phone_number = '01 234 456 78';";

        private const string RENAME_TABLES = @"RENAME TABLE production.UserAccount TO production.UserAccount_original;
                                                    RENAME TABLE production.UserAccount_Obf TO production.UserAccount;";

        public static void Main(string[] args)
        {

            System.Collections.IDictionary environmentVariables = Environment.GetEnvironmentVariables(EnvironmentVariableTarget.Process);
            if (environmentVariables.Count<1)
            {
                throw new Exception("Environment Arguments do not exist");
            }

            string outputLevel = Environment.GetEnvironmentVariable("OUTPUT_LEVEL");

            if (!String.IsNullOrEmpty(outputLevel))
            {
                foreach (object key in environmentVariables.Keys)
                {
                    Console.WriteLine("====================================");
                    Console.WriteLine(key);
                    Console.WriteLine(environmentVariables[key]);
                    Console.WriteLine("====================================");
                }
            }

            


            string redisServer = Environment.GetEnvironmentVariable("REDIS");
            string rdsMysqlServer = Environment.GetEnvironmentVariable("RESTORE_DB_NAME")+ Environment.GetEnvironmentVariable("RDS_DEV_MYSQL_SERVER_SUFFIX");
            string rdsMysqlDatabase = Environment.GetEnvironmentVariable("RDS_MYSQL_DATABASE");
            string rdsMysqlUser = Environment.GetEnvironmentVariable("RDS_MYSQL_USER");
            string rdsMysqlPassword = Environment.GetEnvironmentVariable("RDS_MYSQL_PASSWORD");


            Console.WriteLine(redisServer);
            Console.WriteLine(rdsMysqlServer);
            Console.WriteLine(rdsMysqlUser);
            Console.WriteLine(rdsMysqlPassword);

            Console.WriteLine("Initialising MySQL RDS connection");
            string mySQLConn = "server=" + rdsMysqlServer+ "; database=" + rdsMysqlDatabase + "; uid=" + rdsMysqlUser + ";pwd=" + rdsMysqlPassword;

            var dbFactory = new OrmLiteConnectionFactory(mySQLConn, MySqlDialectProvider.Instance);

            //Non-intrusive: All extension methods hang off System.Data.* interfaces
            try
            {
                IDbConnection dbConn = dbFactory.OpenDbConnection();
                IDbCommand dbCmd = dbConn.CreateCommand();
                dbCmd.CommandText = USER_ACCOUNT_OBF_TABLE;
                var result = dbCmd.ExecuteNonQuery();

                Console.WriteLine(String.Format("Creating table production.UserAccount_Obf with {0} rows", result));

                dbCmd.CommandText = USER_TEMP_TABLE;
                result = dbCmd.ExecuteNonQuery();

                Console.WriteLine(String.Format("Creating table production.user_temp with {0} rows", result));

                dbCmd.CommandText = COMPANY_TEMP_TABLE;
                result = dbCmd.ExecuteNonQuery();

                Console.WriteLine(String.Format("Creating table production.company_temp  with {0} rows", result));

                dbCmd.CommandText = GENERATE_NEW_COMPANY_NAMES;
                result = dbCmd.ExecuteNonQuery();

                Console.WriteLine(String.Format("Generated new company names"));

                dbCmd.CommandText = GENERATE_NEW_COMPANY_EMAIL;
                result = dbCmd.ExecuteNonQuery();

                Console.WriteLine(String.Format("Generated new company emails"));

                dbCmd.CommandText = UPDATE_OBF_TABLE;
                result = dbCmd.ExecuteNonQuery();

                Console.WriteLine(String.Format("Update obf table"));


                Console.WriteLine("Initialising Redis connection");
                Console.WriteLine(redisServer);
                using (RedisClient redisClient = new RedisClient(redisServer))
                {

                    Console.WriteLine("Selecting db3 in Redis");
                    redisClient.ChangeDb(3);

                    foreach (var key in redisClient.ScanAllKeys())
                    {

                        //REmove CrmAuthentication: key value pairs
                        if (key.Contains("CrmAuthentication:"))
                        {
                            Console.WriteLine("Deleteing: ");
                            redisClient.Remove(key);  
                        }

                        if (key.Contains("urn:userauth:"))
                        {
                            Console.WriteLine("urn:userauth: needs updating: ");
                            Console.WriteLine(key.ToString());
                            Console.WriteLine(redisClient.GetValue(key));
                            //redisClient.Set(key, "value");
                            string newJson = processJson(redisClient.GetValue(key), dbConn);
                            Console.WriteLine(newJson);
                            redisClient.SetValue(key, newJson);
                        }

                        if (key.Contains("hash:UserAuth:"))
                        {
                            Console.WriteLine("hash:UserAuth: needs updating: ");
                            Console.WriteLine(key.ToString());
                            //Console.WriteLine(redisClient.GetValue(key));
                            Dictionary<string, string> redisDict = redisClient.GetAllEntriesFromHash(key);
                            //redisClient.Set(key, "value");


                            Dictionary<string, string> outDict = processDictionary(redisDict, dbConn);
                            List<string> keys = new List<string>();
                            try
                            {
                                keys.Add(key);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(String.Format("{0}", ex.Message));
                            }

                            redisClient.RemoveAll(keys);
                            redisClient.SetRangeInHash(key.ToString(), outDict);
                        }

                        Console.WriteLine(key.ToString());
                    }

                }

                dbCmd.CommandText = RENAME_TABLES;
                result = dbCmd.ExecuteNonQuery();

                Console.WriteLine(String.Format("Renaming tables"));

                Console.WriteLine("Complete....");

            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("{0}", ex.Message));
            }




            /* var manager = new RedisManagerPool("localhost:6379");
             using (var client = manager.GetClient())
             {
                 client.Set("foo", "bar");
                 Console.WriteLine("foo={0}", client.Get<string>("foo"));
             }*/

            //var allKeys = new List<string>(); allKeys.Add(key);



        }

        private static void ReadSingleRow(IDataRecord record)
        {
            Console.WriteLine(String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
                record[0], record[1], record[2], record[3], record[4], record[5], record[6], record[7], record[8], record[9], record[10]));
        }

        private static bool checkRedisJsonElement(JObject rss, string element)
        {
            try
            {
                Console.WriteLine(rss[element].ToString());
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("email element missing from Json: {0} ", ex.Message));
                return false;
            }
        }

        private static string processJson(string json, IDbConnection dbConn)
        {
            try
            {
                JObject rss = JObject.Parse(json);
                //JObject email = (JObject)rss["email"];
                if (checkRedisJsonElement(rss, "email"))
                {
                    Console.WriteLine(rss["email"].ToString());

                    if (rss["email"].ToString() == "urn:userauth:1930")
                    {
                        Console.WriteLine("Test");
                    }



                    IDbCommand dbCmd = dbConn.CreateCommand();
                    string cmd = "select new_user_email, CONCAT('User_' , user_id) from production.user_temp where user_email = '" + rss["email"].ToString() + "'";
                    dbCmd.CommandText = cmd;
                    IDataReader resultSet = dbCmd.ExecuteReader();

                    //If no match then delete in Redis

                    Console.WriteLine(String.Format("Checking matching emails in MySQL: {0} ", resultSet.ToString()));

                    //JToken email = rss.SelectToken("email");

                    while (resultSet.Read())
                    {
                        IDataRecord record = (IDataRecord)resultSet;
                        if ((string)record[0] == "User_14@Company_11.com")
                        {
                            Console.WriteLine("Waiting");
                            string userAccount = (string)rss["meta"]["UserAccount"];
                            userAccount = userAccount.Replace((string)rss["email"], (string)record[0]);
                            Console.WriteLine(userAccount);
                            rss["meta"]["UserAccount"] = userAccount;
                            //string userAcc = (string)userAccount["userEmail"];
                            //userAccount["userEmail"] = (string)record[0];
                        }

                        rss["displayName"] = (string)record[1];
                        rss["email"] = (string)record[0];
                        rss["userEmail"] = (string)record[0];
                        
                    }

                    resultSet.Close();

                    return rss.ToString();
                }
                else
                {
                    Console.WriteLine("Skipping this Json as it has no matching email (lowercase) element.");
                    return rss.ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("processJson errorr: {0} ", ex.Message));
                throw ex;
            }

        }

        private static Dictionary<string, string> processDictionary(Dictionary<string, string> inDict, IDbConnection dbConn)
        {

            Dictionary<string, string> outDict = new Dictionary<string, string>();

            IDbCommand dbCmd = dbConn.CreateCommand();

            foreach (KeyValuePair<string, string> item in inDict)
            {
                Console.WriteLine("Key: {0}, Value: {1}", item.Key, item.Value);


                string cmd = "select new_user_email from production.user_temp where user_email = '" + item.Key.ToString() + "'";
                dbCmd.CommandText = cmd;
                IDataReader resultSet = dbCmd.ExecuteReader();
                //If no match then delete in Redis

                Console.WriteLine(String.Format("Checking matching emails in MySQL: {0} ", item.Key.ToString()));

                //JToken email = rss.SelectToken("email");

                try
                {

                    while (resultSet.Read())
                    {
                        IDataRecord record = (IDataRecord)resultSet;
                        outDict.Add(record[0].ToString(), item.Value.ToString());
                        Console.WriteLine(String.Format("Found matching emails in MySQL: {0} ", record[0].ToString()));
                        //item.Value = (string)record[0];
                        //change this to add to new dictionary
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(String.Format("{0}", ex.Message));
                }

                //If email is not in database don't add it back to Redis, so no else block

                resultSet.Close();
            }

            return outDict;

        }

    }
}
